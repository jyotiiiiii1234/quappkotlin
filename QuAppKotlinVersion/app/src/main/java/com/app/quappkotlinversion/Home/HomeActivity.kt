package com.app.quappkotlinversion.Home

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.quappkotlinversion.EmptyDataAdapter
import com.app.quappkotlinversion.R
import com.app.quappkotlinversion.Utility.AppData
import com.app.quappkotlinversion.Utility.SharedPreference
import com.app.quappkotlinversion.Utility.Utility
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_home.*
import java.io.IOException
import java.util.*

class HomeActivity : AppCompatActivity() {

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var lastLocation: Location? = null
    var homeDataList: List<CustHomeModel>? = null
    var requestQueue: RequestQueue? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        requestQueue = Volley.newRequestQueue(this)

        val sharedPreference:SharedPreference= SharedPreference(this)


        tv_cust_name.text = sharedPreference.getValueString("name")!!

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        requestedPermission()

        categoryServiceList()

    }

    private fun categoryServiceList() {
        val setToolbarTitle = Utility()
        setToolbarTitle.showProgress(this)
        val stringRequest = StringRequest(
                Request.Method.POST,AppData.ApplicationAPI.CustCategory ,
                Response.Listener { response ->
                    setToolbarTitle.hideProgress(this@HomeActivity)
                    homeDataList = ArrayList<CustHomeModel>()
                    val homeResponse: HomeResponse = Gson().fromJson<HomeResponse>(
                        response,
                        HomeResponse::class.java
                    )
                    homeDataList = homeResponse.getData() as List<CustHomeModel>?
                  //  setToolbarTitle.hideProgress(this@HomeActivity)
                    if (homeDataList != null && homeDataList!!.size > 0) {
                     //   setToolbarTitle.hideProgress(this@HomeActivity)
                        setAdapter(homeDataList!!)
                    } else {
                        rv.setAdapter(
                            EmptyDataAdapter(this@HomeActivity, "No data found", R.drawable.no_data_available, 1)
                        )
                    }
                }, Response.ErrorListener {
                setToolbarTitle.hideProgress(this)
                })
        requestQueue!!.add(stringRequest)
    }

    private fun setAdapter(homeDataList: List<CustHomeModel>) {

//        rv.layoutManager = LinearLayoutManager(this@HomeActivity)
//        val parentadapter = HomeAdapter(homeDataList, this)
//        rv.adapter = parentadapter

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rv.layoutManager = layoutManager

        val adapter= HomeAdapter(this, homeDataList)
        rv.adapter = adapter
    }

    private fun requestedPermission() {
        if (!checkPermissions()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions()
            }
        }
        else {
            getLastLocation()
        }
    }
    private fun getLastLocation() {

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient?.lastLocation!!.addOnCompleteListener(this) { task ->
            if (task.isSuccessful && task.result != null) {
                lastLocation = task.result
                AppData.values.lat= (lastLocation)!!.latitude.toString()
                AppData.values.lat = (lastLocation)!!.longitude.toString()
                val lat=(lastLocation)!!.latitude.toString()
                val lon=(lastLocation)!!.longitude.toString()


                //location update
                getAddressByLatLong(lat, lon)
            }
            else {
                Log.w(TAG, "getLastLocation:exception", task.exception)
            //    showMessage("No location detected. Make sure location is enabled on the device.")
            }
        }
    }

    private fun getAddressByLatLong(lat: String, lon: String) {

        val geocoder: Geocoder
        val addresses: List<Address>?

        geocoder = Geocoder(this, Locale.getDefault())

        try {
            addresses = geocoder.getFromLocation(
                lat.toDouble(),
                lon.toDouble(),
                1
            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses != null && addresses.size > 0) {
                val address = addresses[0]
                // sending back first address line and locality
                val current_location =
                    address.getAddressLine(0) + ", " + address.locality
                if (current_location != null) {
                    val setToolbarTitle = Utility()
                    tv_location.text = current_location
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    private fun showSnackbar(
        mainTextStringId: String, actionStringId: String,
        listener: View.OnClickListener
    ) {
        Toast.makeText(this@HomeActivity, mainTextStringId, Toast.LENGTH_LONG).show()
    }
    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }
    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
            this@HomeActivity,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }
    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")
            showSnackbar("Location permission is needed for core functionality", "Okay",
                View.OnClickListener {
                    startLocationPermissionRequest()
                })
        }
        else {
            Log.i(TAG, "Requesting permission")
            startLocationPermissionRequest()
        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            when {
                grantResults.isEmpty() -> {
                    // If user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.i(TAG, "User interaction was cancelled.")
                }
                grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                    // Permission granted.
                    getLastLocation()
                }
                else -> {
                    showSnackbar("Permission was denied", "Settings",
                        View.OnClickListener {
                            // Build intent that displays the App settings screen.
                            val intent = Intent()
                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts(
                                "package",
                                Build.DISPLAY, null
                            )
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        }
                    )
                }
            }
        }
    }
    companion object {
        private val TAG = "LocationProvider"
        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }


}