package com.app.quappkotlinversion.Utility

import android.app.Activity
import android.app.ProgressDialog

 class Utility {

    private var progressDialog: ProgressDialog? = null

    fun showProgress(activity: Activity) {
       progressDialog = ProgressDialog(activity)
       progressDialog!!.setMessage("Please wait")
        progressDialog!!.show()
        progressDialog!!.setCancelable(false)
    }

    fun hideProgress(activity: Activity) {
      progressDialog!!.dismiss()
    }

}