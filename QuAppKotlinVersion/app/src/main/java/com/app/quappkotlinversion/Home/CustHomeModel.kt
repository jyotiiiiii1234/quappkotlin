package com.app.quappkotlinversion.Home

import com.google.gson.annotations.SerializedName

class CustHomeModel {

    @SerializedName("category_id")
    private val categoryId: String? = null

    @SerializedName("spend_time")
    private val spendTime: String? = null

    @SerializedName("name")
    private val name: String? = null

    fun getCategoryId(): String? {
        return categoryId
    }

    fun getSpendTime(): String? {
        return spendTime
    }

    fun getName(): String? {
        return name
    }
}