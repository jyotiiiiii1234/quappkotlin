package com.app.quappkotlinversion.Login

import com.google.gson.annotations.SerializedName

public class CustLoginData {

    @SerializedName("password")
    private var password: String? = null

    @SerializedName("text_password")
    private var textPassword: String? = null

    @SerializedName("address")
    private var address: String? = null

    @SerializedName("latitude")
    private var latitude: String? = null

    @SerializedName("name")
    private var name: String? = null

    @SerializedName("mobile")
    private var mobile: String? = null

    @SerializedName("fcm_token")
    private var fcmToken: Any? = null

    @SerializedName("created_at")
    private var createdAt: String? = null

    @SerializedName("id")
    private var id: String? = null

    @SerializedName("modified_at")
    private var modifiedAt: String? = null

    @SerializedName("longitude")
    private var longitude: String? = null

    @SerializedName("status")
    private var status: String? = null

    fun getPassword(): String? {
        return password
    }

    fun getTextPassword(): String? {
        return textPassword
    }

    fun getAddress(): String? {
        return address
    }

    fun getLatitude(): String? {
        return latitude
    }

    fun getName(): String? {
        return name
    }

    fun getMobile(): String? {
        return mobile
    }

    fun getFcmToken(): Any? {
        return fcmToken
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun getId(): String? {
        return id
    }

    fun getModifiedAt(): String? {
        return modifiedAt
    }

    fun getLongitude(): String? {
        return longitude
    }

    fun getStatus(): String? {
        return status
    }

    fun setMobile(mobile: String?) {
        this.mobile = mobile
    }


    fun setPassword(password: String?) {
        this.password = password
    }

    fun setTextPassword(textPassword: String?) {
        this.textPassword = textPassword
    }

    fun setAddress(address: String?) {
        this.address = address
    }

    fun setLatitude(latitude: String?) {
        this.latitude = latitude
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun setFcmToken(fcmToken: Any?) {
        this.fcmToken = fcmToken
    }

    fun setCreatedAt(createdAt: String?) {
        this.createdAt = createdAt
    }

    fun setId(id: String?) {
        this.id = id
    }

    fun setModifiedAt(modifiedAt: String?) {
        this.modifiedAt = modifiedAt
    }

    fun setLongitude(longitude: String?) {
        this.longitude = longitude
    }

    fun setStatus(status: String?) {
        this.status = status
    }
}