package com.app.quappkotlinversion.Login

import com.google.gson.annotations.SerializedName

public class CustomerLoginModel {

    @SerializedName("result")
    private val result = 0

    @SerializedName("data")
    private val data: List<CustLoginData?>? = null

    @SerializedName("message")
    private val message: String? = null

    fun getResult(): Int {
        return result
    }

    fun getData(): List<CustLoginData?>? {
        return data
    }

    fun getMessage(): String? {
        return message
    }




 }