package com.app.quappkotlinversion

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class EmptyDataAdapter(
    var context: Context,
    var content: String,
    var imageId: Int,
    var type: Int
) :
    RecyclerView.Adapter<EmptyDataAdapter.EmptyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EmptyViewHolder {
        return EmptyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.empty_data_item, parent, false)
        )
    }

    override fun onBindViewHolder(
        holder: EmptyViewHolder,
        position: Int
    ) {

    }

    override fun getItemCount(): Int {
        return 1
    }

    inner class EmptyViewHolder(itemView: View?) :
        RecyclerView.ViewHolder(itemView!!) {

    }

}