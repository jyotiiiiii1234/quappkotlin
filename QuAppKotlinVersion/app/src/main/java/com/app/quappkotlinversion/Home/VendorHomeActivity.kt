package com.app.quappkotlinversion.Home


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.quappkotlinversion.R

class VendorHomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vendor_home)
    }
}