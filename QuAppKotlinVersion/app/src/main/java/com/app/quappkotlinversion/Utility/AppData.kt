package com.app.quappkotlinversion.Utility

public class AppData {

    object ApplicationAPI{

        val commonUrl: String = "http://queue.kazma.co.in/Webservice/"


        val CustomerLogin: String =commonUrl+"customer_login"
        val VendorLogin: String =commonUrl+"vendor_login"
        val CustCategory: String =commonUrl+"category_list"

    }

    object values{

        var lat = ""
        var lon = ""
        var catID = ""
    }
}