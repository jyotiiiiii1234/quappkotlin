package com.app.quappkotlinversion.Home

import com.google.gson.annotations.SerializedName

class HomeResponse {

    @SerializedName("result")
    private val result = 0

    @SerializedName("data")
    private val data: List<CustHomeModel?>? = null

    @SerializedName("message")
    private val message: String? = null

    fun getResult(): Int {
        return result
    }

    fun getData(): List<CustHomeModel?>? {
        return data
    }

    fun getMessage(): String? {
        return message
    }
}