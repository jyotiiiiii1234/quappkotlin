package com.app.quappkotlinversion.Splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.app.quappkotlinversion.Home.HomeActivity
import com.app.quappkotlinversion.Login.LoginActivity
import com.app.quappkotlinversion.R
import com.app.quappkotlinversion.Utility.SharedPreference

class SplashActivity : AppCompatActivity() {

    private val SPLASH_TIME_OUT:Long = 3000 // 1 sec

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sharedPreference: SharedPreference = SharedPreference(this)
            Handler().postDelayed({
                // This method will be executed once the timer is over
                // Start your app main activity
                if(sharedPreference.getValueString("name")!=null){
                    startActivity(Intent(this,HomeActivity::class.java))
                    finish()
                }else{
                    startActivity(Intent(this,LoginActivity::class.java))
                    finish()
                }


            }, SPLASH_TIME_OUT)
        }

}