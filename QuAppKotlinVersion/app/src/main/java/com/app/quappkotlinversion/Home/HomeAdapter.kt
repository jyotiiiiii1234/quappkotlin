package com.app.quappkotlinversion.Home

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.app.quappkotlinversion.R
import com.app.quappkotlinversion.Utility.AppData
import kotlinx.android.synthetic.main.home_adapter_items.view.*

class HomeAdapter(val context: HomeActivity, val homeDataList: List<CustHomeModel>) : RecyclerView.Adapter<HomeAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.home_adapter_items, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return homeDataList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val datalist = homeDataList[position]
        holder.setData(datalist, position)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var currentData: CustHomeModel? = null
        var currentPosition: Int = 0

        init {

            itemView.setOnClickListener {

                val catid: String = currentData!!.getCategoryId()!!
                AppData.values.catID=catid

//                val animationZoomOut = AnimationUtils.loadAnimation(context, R.anim.zoom_out)
//                itemView.startAnimation(animationZoomOut)
//                animationZoomOut.setFillAfter(true)
                Toast.makeText(context, "Category ID : "+catid, Toast.LENGTH_SHORT).show()
            }
        }

        fun setData(datalist: CustHomeModel?, pos: Int) {

            datalist?.let {

                itemView.tvName.text = datalist.getName()

            }

            this.currentData = datalist
            this.currentPosition = pos
        }
    }
}