package com.app.quappkotlinversion.Login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.AuthFailureError
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.quappkotlinversion.Home.HomeActivity
import com.app.quappkotlinversion.Home.VendorHomeActivity
import com.app.quappkotlinversion.R
import com.app.quappkotlinversion.Utility.AppData
import com.app.quappkotlinversion.Utility.SharedPreference
import com.app.quappkotlinversion.Utility.Utility
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class LoginActivity : AppCompatActivity() {


    var loginName: String="user"
    var requestQueue: RequestQueue? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        requestQueue = Volley.newRequestQueue(this)
        click();
    }

    private fun click() {


        val logus = View.OnClickListener {
            rb_vendor.setChecked(true)
            loginName = "vendor"
        }
        rb_vendor.setOnClickListener(logus)

        val logus1 = View.OnClickListener {
            rb_customer.setChecked(true)
            loginName = "user"
        }
        rb_customer.setOnClickListener(logus1)

        bt_login.setOnClickListener() { v ->

            // Toast.makeText(applicationContext,"this is toast message",Toast.LENGTH_SHORT).show()

            val phone : String= et_phone_no.text.toString()
            val password : String= et_password.text.toString()

            if(phone==""){
                Toast.makeText(applicationContext,"Enter Phone No",Toast.LENGTH_SHORT).show()
            }else if (password==""){
                Toast.makeText(applicationContext,"Enter Password",Toast.LENGTH_SHORT).show()
            }else{

                if(loginName=="user"){

                    getCustLogin(phone,password)
                }else{

                    getVendorLogin(phone,password)
                }


            }



        }
    }

    private fun getVendorLogin(phone: String, password: String) {
        val setToolbarTitle = Utility()
        setToolbarTitle.showProgress(this)
        val stringRequestVendorLogin: StringRequest =
            object : StringRequest(
                Method.POST, AppData.ApplicationAPI.VendorLogin,
                Response.Listener { response ->
                    setToolbarTitle.hideProgress(this@LoginActivity)
                    Log.e("cdchdhcbd", "log in response $response")

                    if (response != null) {
                        try {
                            val jobj = JSONObject(response)
                            val result = jobj.getInt("result")
                            val status = jobj.getString("message")
                            if (result == 1) {
                                val jsonObject = jobj.getJSONObject("data")

                                val id=jsonObject.getString("id")
                                val name=jsonObject.getString("name")
                                val mobile=jsonObject.getString("mobile")

                                val sharedPreference:SharedPreference= SharedPreference(this)

                                sharedPreference.save("name", name!!)
                                sharedPreference.save("id",id!!)
                                sharedPreference.save("mobile",mobile!!)


                                Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show()
                                val intent = Intent(this, VendorHomeActivity::class.java)
                                startActivity(intent)
                                finish()
                            } else {
                                Toast.makeText(this, status, Toast.LENGTH_SHORT).show()
                            }
                        } catch (ex: JSONException) {
                            ex.printStackTrace()
                        }
                    }



                }
                , Response.ErrorListener { error ->
                    setToolbarTitle.hideProgress(this@LoginActivity)
                    Toast.makeText(
                        this@LoginActivity,
                        "" + error.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }) {
                @Throws(AuthFailureError::class)
                override fun getParams(): Map<String, String> {
                    val params: MutableMap<String, String> =
                        HashMap()
                    params["mobile"] = phone
                    params["password"] = password
//                    params["fcm_token"] = ssp.getRefreshedToken()
                    Log.e("cdchdhcbd", "login params  $params")
                    return params
                }
            }
        requestQueue!!.add(stringRequestVendorLogin)
    }

    private fun getCustLogin(phone: String, password: String) {
        val setToolbarTitle = Utility()
        setToolbarTitle.showProgress(this)
        val stringRequestVendorLogin: StringRequest =
            object : StringRequest(
                Method.POST, AppData.ApplicationAPI.CustomerLogin,
                Response.Listener { response ->
                    setToolbarTitle.hideProgress(this@LoginActivity)
                    Log.e("cdchdhcbd", "log in response $response")

                    val custLoginResponse: CustomerLoginModel = Gson().fromJson<CustomerLoginModel>(
                        response.toString(), CustomerLoginModel::class.java)

                    if (custLoginResponse.getResult() === 1) {

                        val name= custLoginResponse.getData()!!.get(0)!!.getName()
                        val id= custLoginResponse.getData()!!.get(0)!!.getId()
                        val mobile= custLoginResponse.getData()!!.get(0)!!.getMobile()

                        val sharedPreference:SharedPreference= SharedPreference(this)

                        sharedPreference.save("name", name!!)
                        sharedPreference.save("id",id!!)
                        sharedPreference.save("mobile",mobile!!)



                        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                        Toast.makeText(
                            this@LoginActivity, "" + custLoginResponse.getMessage(), Toast.LENGTH_SHORT).show()
                        } else {
                        Toast.makeText(
                            this@LoginActivity, "" + custLoginResponse.getMessage(), Toast.LENGTH_SHORT).show()
                        }
                    }
                , Response.ErrorListener { error ->
                    setToolbarTitle.hideProgress(this@LoginActivity)
                    Toast.makeText(
                        this@LoginActivity,
                        "" + error.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }) {
                @Throws(AuthFailureError::class)
                override fun getParams(): Map<String, String> {
                    val params: MutableMap<String, String> =
                        HashMap()
                    params["mobile"] = phone
                    params["password"] = password
//                    params["fcm_token"] = ssp.getRefreshedToken()
                    Log.e("cdchdhcbd", "login params  $params")
                    return params
                }
            }
        requestQueue!!.add(stringRequestVendorLogin)
    }



}